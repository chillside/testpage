import { createBrowserRouter } from "react-router-dom";

import Home from "@/routes/Home";
import Post from "@/routes/Post";
import NotFound from "@/routes/NotFound";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
    errorElement: <NotFound />,
  },
  {
    path: "post/:id",
    element: <Post />,
    errorElement: <NotFound />,
  },
]);

export default router;
