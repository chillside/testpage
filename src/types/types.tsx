export type PostType = {
  body: string;
  id: number;
  title: string;
  userid: number;
};
