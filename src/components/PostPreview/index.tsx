import { Link } from "react-router-dom";
import styles from "./styled.module.scss";

const PostPreview = ({ ...props }): JSX.Element => {
  const { title, body, id } = props.data;
  return (
    <div className={styles.post}>
      <h3 className={styles.title}>{title}</h3>
      <div className={styles.body}>{body}</div>
      <Link to={`/post/${id}`} className={styles.readmore}>
        Read more..
      </Link>
    </div>
  );
};

export default PostPreview;
