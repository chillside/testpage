import { useState } from "react";
import styles from "./styled.module.scss";
import PostPreview from "@/components/PostPreview";
import { useSearchParams } from "react-router-dom";

const Pagination = ({ perpage, total, currentPage, setCurrentPage }: any) => {
  let [searchParams, setSearchParams] = useSearchParams();

  const pages = [];
  for (let i = 1; i < Math.ceil(total / perpage); i++) {
    pages.push(i);
  }
  const handleSetPage = (p: number) => {
    setCurrentPage(p);
    //setSearchParams({ page: p.toString() });
    searchParams.set("page", p.toString());
    setSearchParams(searchParams);
  };
  const handleSetPrev = () => {
    if (currentPage > 1) {
      handleSetPage(currentPage - 1);
    }
  };
  const handleSetNext = () => {
    if (currentPage < pages.length) {
      handleSetPage(currentPage + 1);
    }
  };

  return (
    <nav className={styles.pagination}>
      <button
        onClick={handleSetPrev}
        className={`${styles.pagination_button} ${styles.pagination_button_prev}`}
        disabled={currentPage < 2}
      >
        Prev
      </button>
      <div className={styles.pagination_items}>
        {pages.map((item) => {
          return (
            <button
              key={item}
              onClick={() => handleSetPage(item)}
              className={`${styles.pagination_item} ${
                item === currentPage ? styles.pagination_item_active : ""
              }`}
            >
              {item}
            </button>
          );
        })}
      </div>
      <button
        onClick={handleSetNext}
        className={`${styles.pagination_button} ${styles.pagination_button_next}`}
        disabled={currentPage >= pages.length}
      >
        Next
      </button>
    </nav>
  );
};

const Pages = ({ ...props }) => {
  let [searchParams] = useSearchParams();

  const { perpage, content } = props;
  const [currentPage, setCurrentPage] = useState<number>(
    Number(searchParams.get("page")) || 1
  );

  const lastPostIndex = currentPage * perpage;
  const firstPostIndex = lastPostIndex - perpage;
  const currentPostList = content.slice(firstPostIndex, lastPostIndex);

  //const setpage = (page: number) => setCurrentPage(page);

  return (
    <>
      {currentPostList.map((item: any) => {
        return (
          <div key={item.id}>
            <PostPreview data={item} />
          </div>
        );
      })}
      <Pagination
        perpage={props.perpage}
        total={props.total}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
      />
    </>
  );
};

export default Pages;
