import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { PostType } from "@/types/types";
import { Link } from "react-router-dom";
import Loader from "@/components/Loader";
import styles from "./styled.module.scss";

import IconBack from "@/assets/icon-back.svg?react";

const Post = () => {
  const { id } = useParams();
  const [post, setPost] = useState<PostType | null>(null);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    const getApiData = async () => {
      const response = await fetch(
        `https://jsonplaceholder.typicode.com/posts/${id}`
      ).then((response) => response.json());

      setPost(response);
      setLoading(false);
    };

    getApiData();
  }, [id]);

  return (
    <main>
      <div className="container">
        <header className={styles.header}>
          <Link to="/" className={styles.backlink}>
            <IconBack className={styles.iconback} />
            <span>Back to Homepage</span>
          </Link>
        </header>
        {loading ? (
          <Loader />
        ) : (
          <>
            {post ? (
              <article>
                <h1 className={styles.title}>{post.title}</h1>
                <div className={styles.body}>{post.body}</div>
              </article>
            ) : null}
          </>
        )}
      </div>
    </main>
  );
};

export default Post;
