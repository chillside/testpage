import { useState, useEffect } from "react";
import Pages from "@/components/Pages";
import Loader from "@/components/Loader";
import { PostType } from "@/types/types";
import styles from "./styled.module.scss";

const PER_PAGE = 10;

const Home = () => {
  const [posts, setPosts] = useState<PostType[]>([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const getApiData = async () => {
      setLoading(true);
      const response = await fetch(
        "https://jsonplaceholder.typicode.com/posts"
      ).then((response) => response.json());
      setPosts(response);
      setLoading(false);
    };

    getApiData();
  }, []);

  return (
    <main>
      <div className="container">
        <header>
          <h1 className={styles.title}>Blog posts</h1>
        </header>
        <div className={styles.posts}>
          {loading ? (
            <Loader />
          ) : (
            <Pages perpage={PER_PAGE} total={posts.length} content={posts} />
          )}
        </div>
      </div>
    </main>
  );
};

export default Home;
