import styles from "./styled.module.scss";

import { useRouteError } from "react-router-dom";
import { Link } from "react-router-dom";

const NotFound: React.FC = () => {
  const error: unknown = useRouteError();
  return (
    <main className={styles.main}>
      <h1 className={styles.title}>
        {(error as Error)?.message ||
          (error as { statusText?: string })?.statusText}
      </h1>
      <p className={styles.text}>Sorry, an unexpected error has occurred.</p>
      <Link to="/">Visit Homepage</Link>
    </main>
  );
};

export default NotFound;
